package model


case class User(id: Long, name: String, groupId: Seq[Long])

case class Group(id: Long, name: String)
