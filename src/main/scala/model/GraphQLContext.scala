package model

import model.dao.{GroupDao, UserDao}

case class GraphQLContext(groupDao: GroupDao.type, userDao: UserDao.type)