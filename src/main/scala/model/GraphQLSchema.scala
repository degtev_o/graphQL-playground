package model

import sangria.execution.deferred._
import sangria.schema.{Field, ListType, ObjectType}
import sangria.schema._
import sangria.macros.derive._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object GraphQLSchema {
    implicit val GroupType: ObjectType[Unit, Group] = deriveObjectType[Unit, Group](
        AddFields(
            Field("users", ListType(UserType), resolve = c =>  userFetcher.deferRelSeq(userByGroup, c.value.id))
        )
    )
    implicit val UserType: ObjectType[Unit, User] = deriveObjectType[Unit, User](
        ReplaceField("groupId",
            Field("groups", ListType(GroupType), resolve = c => groupFetcher.deferSeq(c.value.groupId))
        )
    )

    val userByGroup = Relation[User, Long]("groupId", e => e.groupId)


    val userFetcher = Fetcher.rel(
        (ctx: GraphQLContext, ids: Seq[Long]) => Future.successful(ctx.userDao.getByIds(ids)),
        (ctx: GraphQLContext, ids: RelationIds[User]) => Future.successful(ctx.userDao.getByGroupIds(ids(userByGroup)))
    )(HasId(_.id))

    val groupFetcher = Fetcher(
        (ctx: GraphQLContext, ids: Seq[Long]) => Future.successful(ctx.groupDao.getByIds(ids))
    )(HasId(_.id))

    val Resolver = DeferredResolver.fetchers(userFetcher, groupFetcher)


    val QueryType = ObjectType(
        "Query",
        fields[GraphQLContext, Unit](
            Field("Users",
                ListType(UserType),
                arguments = List(Argument("id", OptionInputType(ListInputType(LongType)))),
                resolve = c => c.argOpt[Seq[Long]]("id") match {
                    case Some(x) => userFetcher.deferSeq(x)
                    case None => c.ctx.userDao.getAll
                }
            ),
            Field("Groups",
                ListType(GroupType),
                arguments = List(Argument("id", OptionInputType(ListInputType(LongType)))),
                resolve = c => c.argOpt[Seq[Long]]("id") match {
                    case Some(x) => groupFetcher.deferSeq(x)
                    case None => c.ctx.groupDao.getAll
                }
            )
        )
    )

    val MutationType = ObjectType(
        "Mutation",
        fields[GraphQLContext, Unit](
            Field("createUser",
                UserType,
                arguments = List(Argument("name", StringType), Argument("groupIds", ListInputType(LongType))),
                resolve = c => c.ctx.userDao.create(User(0, c.arg[String]("name"), c.arg[Seq[Long]]("groupIds")))
            ),
            Field("updateUser",
                IntType,
                arguments = List(Argument("id", LongType), Argument("name", StringType), Argument("groupIds", ListInputType(LongType))),
                resolve = c => c.ctx.userDao.update(User(c.arg[Long]("id"), c.arg[String]("name"), c.arg[Seq[Long]]("groupIds")))
            ),
            Field("deleteUser",
                IntType,
                arguments = List(Argument("id", LongType)),
                resolve = c => c.ctx.userDao.delete(c.arg[Long]("id"))
            ),
            Field("createGroup",
                GroupType,
                arguments = List(Argument("name", StringType)),
                resolve = c => c.ctx.groupDao.create(Group(0, c.arg[String]("name")))
            ),
            Field("updateGroup",
                IntType,
                arguments = List(Argument("id", LongType), Argument("name", StringType)),
                resolve = c => c.ctx.groupDao.update(Group(c.arg[Long]("id"), c.arg[String]("name")))
            ),
            Field("deleteGroup",
                IntType,
                arguments = List(Argument("id", LongType)),
                resolve = c => c.ctx.groupDao.delete(c.arg[Long]("id"))
            )
        )
    )

    val SchemaDefinition = Schema(QueryType, Some(MutationType))
}