package model.dao

import model.Group


object GroupDao {
    private var idCounter = 2
    private var db = Seq(
        Group(1, "Administrators"),
        Group(2, "Users")
    )

    def getAll = db

    //def getById(id: Long) = db.find(_.id == id)

    def getByIds(ids: Seq[Long]) = db.filter(c => ids.contains(c.id))

    def create(group: Group) = {
        idCounter += 1
        val e = group.copy(id = idCounter)
        db = db :+ e
        e
    }

    def update(group: Group): Int =
        db.indexWhere(_.id == group.id) match {
            case -1 => throw new NoSuchElementException
            case i => db = db.updated(i, group); 1
        }

    def delete(id: Long): Int =
        db.find(_.id == id) match {
            case None => throw new NoSuchElementException
            case Some(_) => db = db.filterNot(_.id == id); 1
        }
}
