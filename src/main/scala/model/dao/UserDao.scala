package model.dao

import model.User


object UserDao {
    private var idCounter = 5
    private var db = Seq(
        User(1, "Martin Odersky", Seq(1,2)),
        User(2, "James Gosling", Seq(1,2)),
        User(3, "Bjarne Stroustrup", Seq(1,2)),
        User(4, "Brendan Eich", Seq(2)),
        User(5, "Linus Benedict Torvalds", Seq(2)),
    )

    def getAll = db

    //def getById(id: Long) = db.find(_.id == id)

    def getByIds(ids: Seq[Long]) = db.filter(user => ids.contains(user.id))

    def getByGroupIds(ids: Seq[Long]) = db.filter(user => user.groupId.exists(gid => ids.contains(gid)))

    def create(user: User) = {
        idCounter += 1
        val e = user.copy(id = idCounter)
        db = db :+ e
        e
    }

    def update(user: User): Int =
        db.indexWhere(_.id == user.id) match {
            case -1 => throw new NoSuchElementException
            case i => db = db.updated(i, user); 1
        }

    def delete(id: Long): Int =
        db.find(_.id == id) match {
            case None => throw new NoSuchElementException
            case Some(_) => db = db.filterNot(_.id == id); 1
        }
}
